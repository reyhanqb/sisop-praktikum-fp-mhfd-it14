#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <jansson.h>
#include <time.h>
#include <string.h>

void dump_database(char *dir_name, FILE *backup_file)
{
    DIR *d;
    struct dirent *dir;
    d = opendir(dir_name);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (dir->d_type == DT_REG)
            { // Regular file
                char file_name[100];
                sprintf(file_name, "%s/%s", dir_name, dir->d_name);
                json_error_t error;
                json_t *root = json_load_file(file_name, 0, &error);

                if (!root)
                {
                    fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
                    return;
                }

                if (!json_is_array(root))
                {
                    fprintf(stderr, "error: root is not an array\n");
                    json_decref(root);
                    return;
                }

                size_t i, size;
                json_t *data, *value;
                const char *key;

                char table_name[100];
                strncpy(table_name, dir->d_name, strlen(dir->d_name) - 5); // Subtract 5 to remove ".json"
                table_name[strlen(dir->d_name) - 5] = '\0';

                fprintf(backup_file, "DROP TABLE %s;\n", table_name);

                // For CREATE TABLE statement
                fprintf(backup_file, "CREATE TABLE %s (", table_name);
                json_t *first_row = json_array_get(root, 0);
                json_object_foreach(first_row, key, value)
                {
                    fprintf(backup_file, "%s string, ", key); 
                }
                fseek(backup_file, -2, SEEK_CUR); 
                fprintf(backup_file, ");\n");

                json_array_foreach(root, i, data)
                {
                    fprintf(backup_file, "INSERT INTO %s (", table_name);
                    size_t count = 0, total = json_object_size(data);
                    json_object_foreach(data, key, value)
                    {
                        count++;
                        // Dump the data 
                        fprintf(backup_file, "%s%s", json_string_value(value), count < total ? ", " : "");
                    }
                    fprintf(backup_file, ");\n");
                }

                json_decref(root);
            }
        }
        closedir(d);
    }
}
int main()
{
    DIR *d;
    struct dirent *dir;
    d = opendir("./database");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            if (dir->d_type == DT_DIR && strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
            { // Check if it's a directory
                char dir_name[100];
                sprintf(dir_name, "./database/%s", dir->d_name);

                char backup_file_name[100];
                time_t t = time(NULL);
                struct tm tm = *localtime(&t);
                sprintf(backup_file_name, "%s_%04d%02d%02d%02d%02d%02d.backup", dir->d_name, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

                FILE *backup_file = fopen(backup_file_name, "w");
                dump_database(dir_name, backup_file);
                fclose(backup_file);

                char zip_command[150];
                sprintf(zip_command, "zip %s.zip %s", backup_file_name, backup_file_name);
                system(zip_command); // Run the zip command

                // rm file backup
                remove(backup_file_name);
            }
        }
        closedir(d);
    }

    return 0;
}